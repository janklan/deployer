<?php

/**
 * janklan/deployer
 * 
 * This class contains configuration for the deployer script
 * 
 * Example file should never be used directly, although you can extend it in
 *  your own configuration files to provide easier management of multiple 
 *  configuration environments.
 * 
 * You should never put your configuration files containing access passwords
 *  and/or any other sensitive informations into your versioned tree.
 * 
 * Every deployment environmnent should use it's own configuration.
 *
 * @author Jan Klan <jan@beatee.org>
 */

abstract class DeployConfig extends DeployConfigAbstract {

	const BASEDIR = '/var/www/example.com/';

	/**
	 * Enable or disable debug mode
	 * 
	 * In debug mode no files are deleted and 'git reset --hard' is not sent
	 * 
	 * @var bool
	 */
	
	public static $debug		= true;
	
	/**
	 * Job name is used in the email message sent when deployment is finished
	 * 
	 * @var string
	 */
	
	public static $jobName		= 'Example deployment configuration';
	
	/**
	 * Absolute path to the git binary
	 * 
	 * @var string
	 */
	
	public static $gitBinary	= '/usr/bin/git';
	
	/**
	 * Enable or disable sending 'git reset --hard' command before pulling changes
	 * 
	 * Set to false if you don't want to reset all changes to the working directory 
	 *  before pulling updates	 * 
	 * 
	 * @var bool
	 */
	
	public static $gitReset		= false;
	
	/**
	 * Origin branch containing files you want to pull to your local directory
	 * 
	 * @var string
	 */
	
	public static $gitBranch	= 'public/stable';
	
	/**
	 * Path to your local git repository
	 * 
	 * @var string
	 */
	
	public static $appDir		= self::BASEDIR;
	
	/**
	 * SMTP host used to send the deployment report
	 * 
	 * @var string
	 */
	
	public static $smtpHost		= 'smtp.example.com:25'; // or null to use internal mail() function
	
	/**
	 * User name for SMTP authorization
	 * 
	 * @var string
	 */
	
	public static $smtpUser		= null;
	
	/**
	 * User password for SMTP authorization
	 * 
	 * @var string
	 */
	
	public static $smtpPass		= null;
	
	/**
	 * Sender address of the deployment report
	 * 
	 * @var string
	 */
	
	public static $smtpFrom		= 'deployment@example.com';
	
	/**
	 * List of recipients which will recieve the deployment report
	 * 
	 * @var array
	 */
	
	public static $mailTo		= array(
		'john.doe@example.com'
	);
	
	/**
	 * List of memcached servers which need to be flushed when the pull end
	 * 
	 * @var array
	 */
	
	public static $flushMemcache = array(
//		array(
//			'ip'	=> '127.0.0.1',
//			'port'	=> '11211'
//		)
	);
	
	/**
	 * List of directories which need to be flushed when the pull ends
	 * @var array
	 */
	
	public static $flushDirs = array(
//		'temp/cache'
	);

	/**
	 * List of commands to be executed inside a working directory specified as a key of the 2d array
	 * @var array
	 */

	public static $executeCommands = array(
//		self::BASEDIR => array(
//			'/usr/local/bin/composer update 2>&1',
//			'/usr/local/bin/node node_modules/bower/lib/bin/bower.js update 2>&1',
//			'/usr/local/bin/node node_modules/gulp/bin/gulp.js build-all 2>&1',
//		)
	);
    
    /**
     * Set to true if your webserver use is not owner of the files you are adding
     *  or simply when you need to run the git command under a different user
     *  than the one running web server
     * 
     * @var bool
     */
    
    public static $requireCliExecution = false;
    
    /**
     * CLI execution validates the user name which needs to be used to execute
     *  the cron job correctly
     * 
     * @var string 
     */
    
    public static $requireUserForDeploy = null;
}