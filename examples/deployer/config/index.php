<?php

// In case of allowed directory listing and allowed access to this directory
//  we at least die with dignity. 

header('HTTP/1.0 403 Forbidden');
exit;