<?php

/**
 * Deployer
 *
 * Deployer is automated script primarily built for BitBucket's POST hook.
 *
 * @author Jan Klan <jan@beatee.org>
 * @package Deployer
 */

define ('DEPLOYER_BASEDIR', dirname(__FILE__));

require DEPLOYER_BASEDIR.'/inc/functions.php';
require DEPLOYER_BASEDIR.'/inc/autoload.php';
require DEPLOYER_BASEDIR.'/inc/class.parsedown.php';
require DEPLOYER_BASEDIR.'/inc/class.deployconfigabstract.php';
/**
 * This script can execute only when it knows, where to look for config files.
 */

if (!defined('DEPLOYER_CONFIGDIR') || trim(DEPLOYER_CONFIGDIR) == '') {
    echo logMessage('The DEPLOYER_CONFIGDIR value is not specified. Unable to load configuration.', 'error');
    exit;
}

/**
 * This is the directory where git pull requests are store in case of two-step
 *  deplyoment process.
 */

$tempDir = realpath(DEPLOYER_CONFIGDIR.'/../temp');

if (IS_CLI) {

    /**
     * If the script is called via CLI, we work in a different mode - we assume
     *  that there are some pushes recorded in $tempDir repository as BitBucket
     *  or the end user have requested them.
     *
     * Every such attempt is stored in a file, which has the same file name as
     *  a requested (deploy job|config file).
     *
     * There is either a word "manual" in the file or full JSON string extraced
     *  from the $_POST['payload'] array when BitBucket attempted to issue the
     *  pull procedure.
     *
     * It is possible that multiple requests have been issued since this script
     *  ran for the last time. Therefore every single deploy job is stored in
     *  it's respective file and we can deploy them all at once as separate pull
     *  jobs.
     *
     * The .json file is saved only in case the push was within the tracked branch
     *  so there are no useless steps made.
     */
    // Load all requests
    foreach (glob($tempDir.'/*.json') as $pendingFile) {
        // Extract the configuration file name
        $filename = pathinfo($pendingFile, PATHINFO_FILENAME);

        if (isValidFilename($filename)) {
            // Load the file data to determine BitBucket request / manual request
            $payload = file_get_contents($pendingFile);
            if ($payload !== 'manual' && $payload = json_decode($pendingFile)) {
                // Populate the $_POST field in case of BitBucket request
                $_POST['payload'] = $payload;
            } else {
                // Or ensure such field does not exist in the opposite case
                unset($_POST['payload']);
            }

            // The environment is reday, let's roll
            deploy($filename);
        }

        // The request is processed, destroy it!
        unlink($pendingFile);
    }

} else {

    /**
     * Deployer expects the mod_rewrite to funnel all requests to this script and
     *  then it extracts the actual configuration file from the URL.
     *
     * See readme.md for more info.
     */

    if (isset($_GET['config'])) {
        $filename = $_GET['config'];
    } else {
        $filename = pathinfo($_SERVER['REQUEST_URI'], PATHINFO_FILENAME);
    }

    if (isValidFilename($filename)) {

        // The config file defines everything we need to know for the specified
        //  deploy job
        require_once DEPLOYER_CONFIGDIR.'/config.' . $filename . '.php';

        /**
         * Just deploy the damn thing if you did not request CLI execution because
         *  your filesystem rights and configuration allows the web server user to
         *  do git pull and other things above the directory tree.
         *
         * Which is a security hazard btw!
         */

        if (!DeployConfig::$requireCliExecution) {
            deploy($filename);
        } else {

            // Determine the pull request json file name in advance
            $pendingFile = $tempDir.'/'.$filename.'.json';

            // And check if we can store the request.
            if (is_writable($tempDir)) {

                /**
                 * If $_POST['payload'] is stored, we need to store it into $pendingFile
                 *  as this is clearly a BitBucket type of a request
                 */

                if (isset($_POST['payload'])) {
                    $doDeploy = false;
                    $payload = json_decode($_POST['payload']);

                    /**
                     * Bitbucket indicates in which branch the push occurred so we
                     *  just check if there is any branch we care about involved
                     */

                    if (isset($payload->commits) && is_array($payload->commits)) {
                        foreach ($payload->commits as $commit) {

                            // If any of the branches commited was the one we care of
                            if ($commit->branch == DeployConfig::$gitBranch) {
                                // Deployment can happen! yay!
                                $doDeploy = true;
                                break;
                            }
                        }
                    }

                    if ($doDeploy) {
                        $payload = $_POST['payload'];
                    }

                } else {

                    /**
                     * Manual deployment means we try to update the directory structure
                     *  no matter what the current state of the remote repository is.
                     */

                    $doDeploy = true;
                    $payload = 'manual';
                }

                // If the deploy should be done
                if ($doDeploy) {
                    // Schedule it
                    file_put_contents($pendingFile, $payload);
                    // We expect a different user will have to delete the file
                    chmod($pendingFile, 0666);
                    // Log it
                    $message = logMessage('Pull request scheduled for execution', $filename);
                    // And announce it's scheduling via e-mail
                    emailMessage(array($message));
                    exit;
                } else {
                    echo logMessage('Ignoring pull request as it was in a branch we do not track', $filename);
                    exit;
                }
            } else {
                echo 'Error: Unable to write pending deploy request to '.$pendingFile;
                exit;
            }
        }
    }
}