<?php

/**
 * Create a constant which allows to determine whether this is a CLI call
 *  or a web request
 */

define ('IS_CLI', (PHP_SAPI == 'cli'));

/**
 * Returns HTML or plaintext version of a newline based on IS_CLI value
 *
 * @return string
 */

function br()
{
    return IS_CLI ? "\r\n" : "<br>";
}

/**
 * Returns HTML or plaintext version of a horizontal line based on IS_CLI value
 *
 * @return string
 */

function hr()
{
    return IS_CLI ? "\r\n-----------------------\r\n" : "<hr>";
}

/**
 * Message logging
 *
 * Stores any $message to log/$file.log, adds current timestamp and remote
 *  ip address
 *
 * When the $message is an array, it doesn't add timestamp, but joins all array
 *  items with a newline and appends it to the log
 *
 * @param string|array $message Message to be logged
 * @param string $file
 */

function logMessage($message, $file) {
    $logfile = DEPLOYER_BASEDIR.'/tmp/log/'.$file.'.log';

    if (is_array($message)) {
        $message = join("\n", $message)."\n";
    } else {
        $message = '['.date('Y-m-d H:i:s').' '.$_SERVER['REMOTE_ADDR'].'] '.trim($message)."\n";
    }

    if (!file_put_contents($logfile, $message, FILE_APPEND)) {
        header('HTTP/1.0 500 Internal Server Error', true, 500);
        echo 'Unable to write to the log file '.$logfile."<br>\n";
    }

    return $message;
}

/**
 * Recursively removes specified directory's contents, leaving the directory
 *  untouched
 *
 * @param string $dir
 */

function recursiveRmDir($dir)
{
    $iterator = new RecursiveIteratorIterator(
        new \RecursiveDirectoryIterator($dir, \FilesystemIterator::SKIP_DOTS),
        \RecursiveIteratorIterator::CHILD_FIRST
    );

    foreach ($iterator as $filename => $fileInfo) {

        // Skip everything what is a hidden file or a directory
        if (!preg_match('~/\.~', $filename)) {
            if ($fileInfo->isDir()) {
                rmdir($filename);
            } else {
                unlink($filename);
            }
        }
    }
}

/**
 * Sends the deployment result to defined recipients via set SMTP server
 * @param array $deployResult
 */

function emailMessage($deployResult)
{
    $Parsedown = new Parsedown();
    $deployResult = array_map('rtrim', $deployResult);

    // Set SMTP mailer's configuration
    $mail = new PHPMailer();

    if (null !== DeployConfig::$smtpHost) {
        $mail->isSMTP();

        $host = explode(':', DeployConfig::$smtpHost);
        $mail->Host     = $host[0];

        if (isset($host[1]) && is_numeric($host[1])) {

            $mail->Port = $host[1];
        }

        if (null !== DeployConfig::$smtpUser) {
            $mail->SMTPAuth = true;
            $mail->Username = DeployConfig::$smtpUser;
            $mail->Password = DeployConfig::$smtpPass;
        } else {
            $mail->SMTPAuth = false;
        }
    }

    $mail->Subject  = DeployConfig::$jobName;
    $mail->setFrom(DeployConfig::$smtpFrom);

    // Add all recipients
    foreach (DeployConfig::$mailTo as $address) {
        $mail->addAddress($address);
    }

    // Add HTML version of the deploy report
    $mail->msgHTML(
        $Parsedown->text(join("\n", $deployResult))
        . '<hr>$_SERVER<pre>' . print_r($_SERVER, true) . '</pre>'
        . '<hr>$_POST<pre>' . print_r($_POST, true) . '</pre>'
        . '<hr>$_GET<pre>' . print_r($_GET, true) . '</pre>'
    );


    // Add plaintext version of the deploy report
    $mail->AltBody =
        join("\n", $deployResult)
        . "\n\n" . print_r($_SERVER, true)
        . "\n\n" . print_r($_POST, true)
        . "\n\n" . print_r($_GET, true)
    ;


    $mailSent = $mail->send();
    if ($mailSent) {
        echo "Mail has been sent.";
        echo br();
    } else {
        echo "ERROR: Mail has NOT been sent.";
        echo br();
        echo $mail->ErrorInfo;
    }
    echo hr();

    echo $Parsedown->text(join("\n", $deployResult));

    /*
    foreach ($deployResult as $row) {
        echo $row;
        echo br();
    }
    */
}

/**
 * Checks if the specified string is a valid deploy job configuration file name
 *
 * @param string $filename
 * @return boolean
 */

function isValidFilename($filename)
{
    // Just one more security check before we continue
    if (!preg_match('/^[a-z0-9]{6,128}$/i', $filename)) {
        // Generic error report if the deploy fails at this point
        echo logMessage('Invalid deploy script name selected.', 'error');
        return false;

    } else {
        if (!file_exists(DEPLOYER_CONFIGDIR.'/config.' . $filename . '.php')) {
            // Generic error report if the deploy fails at this point
            echo logMessage('Deploy script ' . $filename . ' does not exist.', 'error');
            return false;
        } else {
            return true;
        }
    }
}

function executeCommand($command, $cwd, &$deployResult)
{
    if (trim(getcwd()) == trim($cwd) || chdir($cwd)) {
        // Make sure git works with the proper branch
        $deployResult[] = '- `'.$command.'`';

        foreach (explode("\n", trim(shell_exec($command))) as $outputLine) {
            $deployResult[] = ' - `'.trim($outputLine).'`';
        }

    } else {
        if (!is_dir($cwd)) {
            $deployResult[] = '- WARNING, `'.$command.'` failed - '.$cwd.' does not exist';
        } else {
            $deployResult[] = '- WARNING, `'.$command.'` failed - unable to chdir to '.$cwd;
        }
    }
}

/**
 * Executes the deployment procedure identified by the deployment job name
 *
 * @param string $filename
 * @return boolean
 */

function deploy($filename)
{
    $return = false;

    exec('whoami', $whoami);
    $currentUser = $whoami[0];

    // The config file defines everything we need to know for the specified
    //  deploy job
    require_once DEPLOYER_CONFIGDIR.'/config.' . $filename . '.php';

    $deployResult = array(
        "# Deployer Report",
        "- Started: " . date('j. n. Y H:i:s')
    );

    if (!IS_CLI) {
        $deployResult[] = "- Deploy script URL: " . $_SERVER['REQUEST_URI'];
        $deployResult[] = "- HTTP UA: " . $_SERVER['HTTP_USER_AGENT'];
        $deployResult[] = "- Remote Addr: " . $_SERVER['REMOTE_ADDR'];

        $deployResult[] = "- Process owner: " . exec('whoami').':'.join(',', $whoami);
    }

    if (DeployConfig::$requireUserForDeploy === null
        || DeployConfig::$requireUserForDeploy == $currentUser) {

        if (isset($_POST['payload'])) {
            $doDeploy = false;
            $payload = json_decode($_POST['payload']);

            if (isset($payload->commits) && is_array($payload->commits)) {
                foreach ($payload->commits as $commit) {
                    if ($commit->branch == DeployConfig::$gitBranch) {
                        $deployResult[] = '- Deploy mode: BitBucket push.';
                        $doDeploy = true;
                        break;
                    }
                }
            }

            if (!$doDeploy) {
                echo logMessage('BitBucket push into untracked branch detected, exiting.', $filename);
                return false;
            }
        } else {
            $deployResult[] = '- Deploy mode: Manual execution.';
        }

        $deployResult[] = '- Deploy started at '.date('Y-m-d H:i:s');

        // Every command needs to be executed from the repository
        if (!chdir(DeployConfig::$appDir)) {
            $deployResult[] = '- Changing directory to `' . DeployConfig::$appDir . '` failed.';
        } else {

            $deployResult[] = '';
            $deployResult[] = '## Git';

            $cwd = shell_exec('pwd');
            $deployResult[] = '- Currently running git commands in ' . $cwd;

            // Reset any changes possibly made on the server side to avoid conflicts
            if (DeployConfig::$gitReset) {
                $command = DeployConfig::$gitBinary . ' reset --hard 2>&1';

                // Don't do that if the debug mode is enabled - just report it would have happend
                if (DeployConfig::$debug) {
                    $deployResult[] = '- DEBUG: Skipping command `'.$command.'`';
                } else {
                    executeCommand($command, $cwd, $deployResult);
                }
            }

            executeCommand(DeployConfig::$gitBinary . ' checkout ' . DeployConfig::$gitBranch.' 2>&1', $cwd, $deployResult);
            executeCommand(DeployConfig::$gitBinary . ' pull 2>&1', $cwd, $deployResult);

            // If there are some directories wich contain file cache, they need to be cleared
            $deployResult[] = '';
            $deployResult[] = '## Directory cache flush';

            if (empty(DeployConfig::$flushDirs)) {
                $deployResult[] = 'No cache directories to flush.';
            } else {
                foreach (DeployConfig::$flushDirs as $flushDir) {
                    $flushDirPath = realpath(DeployConfig::$appDir . '/' . $flushDir);

                    if (empty($flushDirPath)) {
                        $deployResult[] = '- WARNING: ' . DeployConfig::$appDir . '/' . $flushDir . ' does not exist.';
                    } else if ($flushDirPath != DeployConfig::$appDir && is_writable($flushDirPath)) {
                        $deployResult[] = '- Removing all contents of directory ' . $flushDirPath;

                        // Actually delete ony when debug mode is disabled
                        if (DeployConfig::$debug) {
                            $deployResult[] = '- DEBUG: Skipping rm -rf ' . $flushDirPath . '/*';
                        } else {
                            recursiveRmDir($flushDirPath);
                        }
                    } else {
                        $deployResult[] = '- WARNING: ' . $flushDirPath . '/* could not be removed.';
                    }
                }
            }

            $deployResult[] = '';
            $deployResult[] = '## Memcache flush';
            // The same for any possible memcache servers
            if (empty(DeployConfig::$flushMemcache)) {
                $deployResult[] = 'No memcache servers to flush.';
            } elseif (!class_exists('memcache')) {
                $deployResult[] = 'WARNING: Memcache is not installed on the server.';
            } else {
                $memcache = new Memcache;
                // Walk through all registered servers
                foreach (DeployConfig::$flushMemcache as $mcServer) {
                    if (DeployConfig::$debug) {
                        $deployResult[] = '- DEBUG: Would be flushing memcached server ' . join(':', $mcServer);
                    } else {
                        // Connect to every one and flush it.
                        $memcache->connect($mcServer['ip'], $mcServer['port']);
                        $memcache->flush();
                        $memcache->close();
                        // Done, let's report!
                        $deployResult[] = '- Flushing memcached server ' . join(':', $mcServer);
                    }
                }
            }

            $deployResult[] = '';
            $deployResult[] = '## Executing commands';

            // Execute specified commands
            if (empty(DeployConfig::$executeCommands)) {
                $deployResult[] = 'No commands to execute.';
            } else {
                foreach(DeployConfig::$executeCommands as $cwd => $commands) {
                    if (DeployConfig::$debug) {
                        foreach ($commands as $command) {
                            $deployResult[] = '- DEBUG: Skipping command `'.$command.'`';
                        }
                    } else {
                        foreach ($commands as $command) {
                            executeCommand($command, $cwd, $deployResult);
                        }
                    }
                }
            }

            $return = true;

        } // if (!chdir(DeployConfig::$appDir)) {
    } else {
        $deployResult[] = 'Executed with user '. $currentUser.', required user '.DeployConfig::$requireUserForDeploy.'. Aborting.';
    }

    $deployResult[] = '';
    $deployResult[] = '<hr>';
    $deployResult[] = 'Deploy finished at '.date('Y-m-d H:i:s');

    // Log all messages to the file
    logMessage($deployResult, $filename);

    // And send the email report.
    emailMessage($deployResult);

    // That's all folks.
    return true;
}
